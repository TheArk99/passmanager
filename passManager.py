#!/usr/bin/env python

#This script will be able to log in, store passwords and emails, and eventualy has them to store them in a more secure way.

#imports:
import sys, os
#import tk
import re
import time
import hashlib
import subprocess
import os.path
from pathlib import Path
from modules import osClear


osClear.clearer()
#creates file to store at ~/.local/share
user = os.getlogin()
passFile= "/home/" + user + "/.local/share/passManager"
passFilePath = Path("/home/" + user + "/.local/share/passManager")

fileXist = os.path.isfile("/home/" + user + "/.local/share/passManager")

if fileXist == True:
    file = open(passFile, "r")
    getPass = file.read()
    file.close()


# Example of how to hash: hashlib.sha512(b"test").hexdigest()
#'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'

def isPass():
    if fileXist == False or getPass == '':
        print("You do not have a login yet...Generating... ")
        os.system("touch " + passFile)
        file_object = open(passFile, 'a')
        osClear.clearer()
        word = input('\nWaht would you like your password to be?... ').encode('utf-8')
        Passwords = hashlib.sha512(word).hexdigest()
        Passwords = str(Passwords)
        file_object.write(Passwords)
        file_object.close()
    else:
        def asking():
            ask = input('What is your password?... ').encode('utf-8')
            inputedHash = hashlib.sha512(ask).hexdigest()
            return inputedHash

        if asking() != getPass:
            osClear.clearer()
            print('Wrong password...Try again...')
            time.sleep(0.7)
            osClear.clearer()
            time.sleep(0.1)
            osClear.clearer()
            asking()

isPass()
